import ssl

import pika
import pika.exceptions


class ConnectionError(Exception):
    """Error related to client → broker connection."""

    pass


class PCM:
    def __init__(
        self,
        hostname="localhost",
        port=5672,
        virtual_host="/",
        username="guest",
        password="guest",
        tls_on=False,
        server_name_indication=False,
    ):
        self._channel = None
        self._connection = None

        if tls_on:
            options = {"context": ssl.create_default_context()}
            if server_name_indication:
                server_hostname = hostname
                if isinstance(server_name_indication, str):
                    server_hostname = server_name_indication
                options["server_hostname"] = server_hostname
            ssl_options = pika.SSLOptions(**options)
        else:
            ssl_options = None

        try:
            self.conn_params = pika.ConnectionParameters(
                host=hostname,
                port=port,
                virtual_host=virtual_host,
                credentials=pika.credentials.PlainCredentials(username, password),
                ssl_options=ssl_options,
            )
        except Exception as e:
            raise ConnectionError("Invalid connection parameters: {}".format(e))

    def open(self):
        try:
            self._connection = pika.BlockingConnection(self.conn_params)
        except Exception as e:
            raise ConnectionError(
                "Unable to connect to broker: {} {}".format(type(e), str(e))
            )
        self._channel = self._connection.channel()
        self._channel.confirm_delivery()

    def close(self):
        try:
            self._connection.close()
        except pika.exceptions.ConnectionClosed:
            pass

    def __enter__(self):
        if self._channel is None or not self._connection.is_open:
            self.open()
        return self

    def __exit__(self, exc_type, exc, trace):
        if self._connection is not None and self._connection.is_open:
            self.close()

    @property
    def channel(self):
        return self._channel

    def get_queue_length(self, queue_name):
        q_declared = self._channel.queue_declare(queue_name, passive=True)
        return q_declared.method.message_count

    def sleep(self, interval):
        self._connection.sleep(interval)

    def publish(
        self,
        exchange,
        routing_key,
        message,
        content_type="text/plain",
        delivery_mode=2,
        # amount of microseconds for the message to live. Must be str,
        # i.e. one minute is "60000"
        expiration=None,
    ):
        try:
            self._channel.basic_publish(
                exchange,
                routing_key,
                message,
                pika.BasicProperties(
                    content_type=content_type,
                    delivery_mode=delivery_mode,
                    expiration=expiration,
                ),
            )
        except pika.exceptions.NackError:
            return False
        else:
            return True

    def qos(self, prefetch_size=0, prefetch_count=0, global_qos=False):
        """Defines quality of service limits."""
        self._channel.basic_qos(
            prefetch_size=prefetch_size,
            prefetch_count=prefetch_count,
            global_qos=global_qos,
        )

    def consume(self, queue, callback, auto_ack=False, consumer_tag=None):
        """Sets up an basic_consume, consumer tag is returned."""
        return self._channel.basic_consume(
            queue=queue,
            on_message_callback=callback,
            auto_ack=auto_ack,
            consumer_tag=consumer_tag,
        )

    def start_consuming(self):
        """Start consuming queues.

        This state is exited when a KeyboardInterrupt is received."""
        try:
            self._channel.start_consuming()
        except KeyboardInterrupt:
            self._channel.stop_consuming()
