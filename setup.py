from setuptools import find_packages, setup

setup(
    name="pika_context_manager",
    use_scm_version=True,
    description="",
    packages=find_packages(),
    python_requires=">=3",
    setup_requires=["setuptools_scm"],
    install_requires=["pika"],
)
