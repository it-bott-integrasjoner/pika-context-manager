# Pika Context Manager

Simply send a message?

    from pika_context_manager import PCM
    with PCM() as pcm:
        r = pcm.publish('exchange', 'routing.key', 'payload')
        if r:
            print('I sent a message!')
        else:
            print('I failed at sending a message :\'(')
